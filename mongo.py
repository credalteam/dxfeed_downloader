import os
from pymongo import MongoClient

MONGODB_CONNECTION = "mongodb://watstock:watstock123@mongo-prod.wtst.io:27017/dxfeed-eod-fx-prices"
MONGODB_DBNAME = MONGODB_CONNECTION.split('/')[-1]


def mongo_save_eod(datas):
    client = None
    db = None

    client = MongoClient(MONGODB_CONNECTION)
    db = client[MONGODB_DBNAME]

    collection = db.stocks

    eod_id = collection.insert_many(datas).inserted_ids
    return eod_id