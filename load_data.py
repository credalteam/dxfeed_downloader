from urllib2 import Request, urlopen
import json
import pandas as pd
import datetime

def load_data(PAIR, START_DATE, END_DATE=None):

    print "Start loading for FX pair %s from date %s" % (PAIR, START_DATE)
    if (END_DATE is None):
        request = Request('https://tools.dxfeed.com/watstock/rest/events.json?events=Candle&symbols=%s{=d}&fromTime=%s' % (PAIR, START_DATE))
    else:
        request = Request('https://tools.dxfeed.com/watstock/rest/events.json?events=Candle&symbols=%s{=d}&fromTime=%s&toTime=%s' % (PAIR, START_DATE, END_DATE))
        # print request.get_full_url()
    response = urlopen(request)
    json_strings = response.read()
    data = json.loads(json_strings)
    print "Finshed!"
    return data

# dict =  load_data('USDJPY', '2017-11-01')
# print dict
# dict = dict['Candle'][PAIR+'{=d}']
# df = pd.DataFrame(dict)
# print df
