from mongo import mongo_save_eod
import pandas as pd

eod = pd.DataFrame.from_csv('CSV/dxfeed-concate-new/dxfeed_eodfx_new.csv')
length = len(eod)

print "Writing start"
dicts = []
for i in range(0, length):
    temp = eod.ix[i]
    dict = temp.to_dict()
    dicts.append(dict)

ids = mongo_save_eod(dicts)
id_len = len(ids)
print "Writed %s object to mongo" % id_len

print "Writing ends"



