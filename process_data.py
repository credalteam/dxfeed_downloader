import load_data as ld
import pandas as pd
import os
import datetime

DATE = datetime.datetime.today().strftime('%Y-%m-%d')
MAIN_PATH = 'CSV/dxfeed-%s/' % DATE

if not os.path.exists(MAIN_PATH):
    os.makedirs(MAIN_PATH)

def fill_unicode_nan(x):
    if x == u'NaN':
        return 0
    else:
        return x


def get_csv(PAIR, START_DATE,END_DATE=None):

    print "Process start for %s" % PAIR
    dict = ld.load_data(PAIR, START_DATE, END_DATE)
    dict = dict['Candle'][PAIR+'{=d}']
    df = pd.DataFrame(dict)

    df['time'] = pd.to_datetime(df['time'], unit='ms')
    df['eventSymbol'] = df['eventSymbol'].apply(lambda x: x[:-4])
    df['vwap'] = df['vwap'].apply(lambda x: fill_unicode_nan(x))
    df = df.fillna(0)

    df = df[:-1]
    df.to_csv(MAIN_PATH + 'eodfx_%s_%s.csv' % (PAIR[:3]+PAIR[4:], START_DATE), sep='\t')

    return df

def transform_df_1(df):
    df_final = pd.DataFrame()
    df_final['ticker'] = df['eventSymbol']
    df_final['ticker'] = df_final['ticker'].apply(lambda x: x[:3] + x[4:])
    df_final['date'] = df['time']
    df_final['open'] = df['open']
    df_final['high'] = df['high']
    df_final['low'] = df['low']
    df_final['close'] = df['close']
    df_final['volume'] = df['volume']
    df_final['exDividend'] = float(0)
    df_final['splitRatio'] = float(0)
    df_final['adjOpen'] = df['open']
    df_final['adjHigh'] = df['high']
    df_final['adjLow'] = df['low']
    df_final['adjClose'] = df['close']
    df_final['adjVolume'] = df['volume']

    return df_final


PAIRS = ['EUR/USD', 'USD/JPY', 'GBP/USD', 'AUD/USD', 'USD/CAD']
START_DATE = '2000-01-01'
END_DATE = None
# END_DATE = '2018-01-03'

dfs = []
dfs_new = []
for pair in PAIRS:
    df = get_csv(pair, START_DATE, END_DATE)
    dfs.append(df)
    dfs_new.append(df.tail(1))

final = pd.concat(dfs, ignore_index=True)
final_new = pd.concat(dfs_new, ignore_index=True)

final1 = final
final1.to_csv('CSV/dxfeed-concate/dxfeed_eodfx.csv', sep='\t')

final2 = transform_df_1(final)
final2.to_csv('CSV/dxfeed-concate-1/dxfeed_eodfx.csv', sep='\t')
final2.to_csv('CSV/dxfeed-concate-2/dxfeed_eodfx.csv')

final_new1 = transform_df_1(final_new)
final_new1.to_csv('CSV/dxfeed-concate-new/dxfeed_eodfx_new.csv')

